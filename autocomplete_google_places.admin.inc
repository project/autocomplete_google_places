<?php

/**
 * @file
 * Admin page callback file for the autocomplete_google_places module.
 */

/**
 * Form builder; Configure autocomplete_google_places settings for this site.
 *
 * @ingroup forms
 *
 * @see system_settings_form()
 */
function google_places_autocomplete_admin_settings() {
  $form['google_places_autocomplete_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google API key'),
    '#description' => t('You can get it from your <a href="https://code.google.com/apis/console">Google Console</a>.'),
    '#default_value' => variable_get('google_places_autocomplete_key', ''),
  );

  

  return system_settings_form($form);
}
